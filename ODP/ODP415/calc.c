#include <stdio.h>

int calc(char *string) {
  int num1, num2, flag;
  char operator;
  char extraInput[120];

  flag = sscanf(string,"%d%c%d%s", &num1, &operator, &num2, extraInput);
  if(flag != 3) {
    return -1;
  }
  if((operator != '+') && (operator != '/') && (operator != '-') && (operator != '*')) {
    return -2;
  } else if(operator == '+') {
    return num1+num2;
  } else if(operator == '-') {
    return num1-num2;
  } else if (operator == '*') {
    return num1*num2;
  } else {
    return num1/num2;
  } 

  return 0;
}
