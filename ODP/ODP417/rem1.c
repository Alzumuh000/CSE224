#include <stdio.h>
#include <string.h>

int rem1(char *in, char *out) {
  int outIndex=0;
  int xFound = 0;

  for(int i = 0; i<strlen(in); i++) {
    if((in[i] != 'x') || (xFound == 1)) {
      out[outIndex++] = in[i];
    } else {
      xFound=1;
    }
  }
  out[outIndex] = '\0';

  return 1;
}

