#include <stdio.h>

int main(int argc, char **argv) {
  char extraInput1[120];
  char extraInput2[120];
  int num1, num2;
  if(argc != 3) {
    printf("ERR\n");
    return 0;
  } 
  int return1 = sscanf(argv[1],"%d%s",&num1,extraInput1);
  int return2 = sscanf(argv[2],"%d%s",&num2,extraInput2);
  if((return1 != 1) || (return2 != 1)) {
    printf("ERR\n");
    return 0;
  } else {
    printf("%d\n",num1+num2);
  }
  return 0;
}
