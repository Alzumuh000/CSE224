#include <stdio.h>
#include <string.h>

int mix(char *A, char *B, char *out) {
  int outLength = (strlen(A)*2) + 1;
  int aIndex = 0;  // Keeps track of index of A array
  int bIndex = 0;  // Keeps track of index of B array

  for(int i = 0; i < outLength; i++) {
    if(i != (outLength - 1)) { // the last index is an edge case and must be filled with \0.
      if((i%2) == 0) {	//If the current index for out is divisible by 2, then put the current Value in A[aIndex]
        out[i] = A[aIndex];
        aIndex = aIndex + 1;
      } else {
        out[i] = B[bIndex];
        bIndex = bIndex + 1;
      } 
    } else {
      out[i] = '\0';
    }     
  }

  return -1;
}
