#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) {
  double input;
  
  // check if commandline argument was entered
  if (argc != 2) {
    printf("ERROR, failure to input number\n");  
    return 0;
  }

  int valid = sscanf(argv[1],"%lf",&input);
  
  // check if a number was entered
  if (valid != 1) {
    printf("ERROR, invalid input\n");
    return 0;
  }
  
  printf("%lf\n", sqrt(input)); 
  return 0;
}
