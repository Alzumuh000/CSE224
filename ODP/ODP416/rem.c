#include <stdio.h>
#include <string.h>

int rem(char *in, char *out) {
  int outIndex = 0;
  for(int i = 0; i < strlen(in); i++) {
    if(in[i] != 'x') {
      out[outIndex] = in[i];
      ++outIndex;
    }
  }
  out[outIndex] = '\0';
  return 0;
}
