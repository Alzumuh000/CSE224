#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) {
  double input;
  char stringInput[120];

  // check if commandline argument was entered
  if (argc != 2) {
    printf("ERR\n");  
    return 0;
  }

  int valid = sscanf(argv[1],"%lf%s",&input, stringInput);
  
  // check if a string  was entered after the number
  if (valid != 1) {
    printf("INV\n");
    return 0;
  }
  
  if (input < 0) {
    printf("NEG\n");
    return 0;
  }

  printf("%lf\n", sqrt(input)); 
  return 0;
}
