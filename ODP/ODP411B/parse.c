#include <stdio.h>

int parse(char *n) {
  char extraInput[120];
  int num;
  int flag = sscanf(n, "%d%s",&num,extraInput);
  if(flag == 1) {
    if(num == 1) {
      return 1;
    } else if(num == 2) {
      return 2;
    } else if(num == 3) {
      return 3;
    } else {
      return -1;
    }
  }
  return -1;
}
