#include <stdio.h>
int start(int argc, char **argv) {
  int num;
  char extraInput[120];
  if(argc != 2) {
    return -1;
  } else {
    int flag = sscanf(argv[1],"%d%s",&num, extraInput);
    if(flag != 1) return -2; 
    if(num < 1) return -3;
    return num;
  } 
}
