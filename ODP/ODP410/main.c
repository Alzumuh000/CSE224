#include <stdio.h>

int pick(int n);

int main() {
 printf("When entering 4 the pick function returns: %d\n",pick(4));
 printf("When entering 19 the pick function returns: %d\n",pick(19));
 printf("When entering 100 the pick function returns: %d\n",pick(100)); 
 printf("When entering 7 the pick function returns: %d\n",pick(7)); 
 printf("When entering 24 the pick function returns: %d\n",pick(24)); 
 printf("When entering 5 the pick function returns: %d\n",pick(5)); 
 return 0;
}
