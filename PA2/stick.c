#include <stdio.h>

int checkInput(int flag, int numSticks);	// Function that checks if input for the number of sticks is valid
			  		 	// returns 0 if invalid input, returns 1 if valid input
void printSticks(int numSticks);	// Function that prints the pile of sticks onto the screen. 

int main(int argc, char **argv) {
  int numSticks, numSticksTaken, flag;
  char turn = 'c';
  char runTimeInput[120];
  char userSticksTaken[120];
  char extraInput[120];
 
  printf("Welcome to the stick game, where players compete against a computer to see who can pick up the last stick in a pile\n");
  printf("After the player specifies how many sticks should be in the pile (must be an integer greater than 20)\n");
  printf("the player takes turns with the computer and can only pick between 1 and 3 sticks each turn\n");
 
/***  Handling Command-line input  ***/
  // Handling extra arguments 
  if(argc > 2) {
    printf("ERROR: Too many commandline inputs\n");
    return 0;
  } 
  
  // Handling of when 1 commandline input is entered
  if(argc == 2) {
    // sscanf allows us to parse given commandline input
    int flag = sscanf(argv[1], "%d%s", &numSticks, extraInput);
    if(checkInput(flag, numSticks) == 0)
      return 0;
  }
  
/***  Handling runtime input  ***/
  // checks if no commandline input is entered
  if(argc == 1) {
    printf("Please enter the number of sticks you would like to play with (positive integer >= 20):\n");
    // reads input for numSticks
    fgets(runTimeInput, 120, stdin);
    // parses previous input
    int flag = sscanf(runTimeInput, "%d%s", &numSticks, extraInput);
    if(checkInput(flag, numSticks) == 0)
      return 0;
  }
  printSticks(numSticks);
  
  // main gameplay loop
  while(numSticks > 0) {
    // Computer turn
    if(turn == 'c') {
      // computer calculating how many sticks to take
      numSticksTaken = numSticks%4;
      if(numSticksTaken == 0)
        numSticksTaken = 1;
	 
      numSticks = numSticks - numSticksTaken;
      printf("Computer turn: computer takes %d sticks\n", numSticksTaken);
      printSticks(numSticks);
      turn = 'u';  // turn switches to user

    // user turn
    } else {
      printf("User turn: please enter the number of sticks you would like to take (must be >= 1 and <= 3)\n");
      // ingesting user input for how many sticks to take
      fgets(userSticksTaken, 120, stdin);
      // parsing user input
      flag = sscanf(userSticksTaken, "%d%s", &numSticksTaken, extraInput);
      // Loop that repeatedly asks user to input valid number for sticks to take
      while((flag != 1) || (numSticksTaken < 1) || (numSticksTaken > 3)) {
	printf("Please enter a valid input:\n");
        fgets(userSticksTaken, 120, stdin);
        flag = sscanf(userSticksTaken, "%d%s", &numSticksTaken, extraInput);
      }
      numSticks = numSticks - numSticksTaken;
      printf("User turn: user takes %d sticks\n",numSticksTaken);
      printSticks(numSticks);
      turn = 'c';
    }
  }
  // prints out the winner of the game
  if(turn == 'u') {
    printf("Winner is Computer!\n");
  } else {
    printf("Winner is User!\n");
  }
  
  return 0;
}

int checkInput(int flag, int numSticks) {
  if(flag != 1) {
    printf("ERROR: Invalid input\n");
    return 0;
  } else if(numSticks < 20) {	// runs when a postive integer is successfully inputted and parsed by sscanf
    printf("ERROR: Too few sticks\n");
    return 0;
  } else {
    return 1;
  }
}

void printSticks(int numSticks) {
  for(int i = 0; i < numSticks; i++) {
    printf("|");
  }
  printf(" (%d)\n", numSticks);
}
