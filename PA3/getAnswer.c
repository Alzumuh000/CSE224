#include "main.h"

char getAnswer() {
  char charInput;	// character that is returned
  char input[120] = "tempval";	// array to store input from fgets

  // while loop continues to run if user enters invalid input
  while ((strlen(input) != 2) || ((input[0] != 'h') && (input[0] != 'l') && input[0] != 'c') ) {
    printf("Please enter either 'h', 'l', 'c': ");
    fgets(input, 120, stdin);
  }
  charInput = input[0];
  return charInput;
}
