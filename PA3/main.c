#include "main.h"

int main(int argc, char **argv) {
  int low, high;	// store low and high values of the range
  int flag1, flag2;	// flags to determine input validity
  int guess; int guessNum = 1;	// guess stores the value of the guess made by the computer; guessNum stores the value of how many guesses have been made by the computer
  char hlc;			// stores value taken from getAnswer()
  char enter = 0;		// stores value entered by user when prompted to press ENTER
  char extraInput1[120];
  char extraInput2[120];
 
  if(argc == 1) {	// 0 commandline inputs, range set to 0-100
    low = 0;
    high = 100;
  } else if(argc == 2) {	// 1 commandline input, range set to 0-high
    flag1 = sscanf(argv[1],"%d%s", &high, extraInput1);	// parsing input. Flag val determines input validity
    if((flag1 != 1) || (high <= 0)) {
      printf("Error, invalid range inputs\n");
      return 0;
    }
    low = 0;
  } else if(argc == 3) {	// 2 commandline inputs, range set to low-high
    flag1 = sscanf(argv[1],"%d%s", &low, extraInput1);
    flag2 = sscanf(argv[2],"%d%s", &high, extraInput2);
	
    if((flag1 != 1) || (flag2 != 1) || (high < 0) || (low < 0) || (high <= low)) {	// checks flag, makes sure range inputs are valid
      printf("Error, invalid range inputs\n");
      return 0;
    }
  }

  intro();	// Prints greetings and instructions  
  printf("Range of numbers for this round is between %d and %d (inclusive)\n", low, high);
  printf("Estimated maximum number of guesses: %d\n", predict(low, high));
  
  // Waits for user to think of a number and enter
  printf("\nPlease think of a number and press ENTER to play\n");
  while(enter != '\n') {
    enter = fgetc(stdin);
  }

  // gameplay loop
  while(1) {
    guess = (high+low)/2;
    printf("Guess %d: %d\n", guessNum, guess);
    
    // prompts user for answer
    hlc = getAnswer();
    if(hlc == 'h') {
      high = guess-1;
    } else if(hlc == 'l') {
      low = guess+1;
    } else {
      printf("\nThe computer has correctly guessed your number!\n");
      return 0;
    } 

    // cheating detection
    if(low > high) {
      printf("Strange behavior detected. Player has either cheated, forgotten their number, or clicked an incorrect option.\n\n**Computer self-destruct in 20 seconds**\n\n");
      return 0;   
    }
    guessNum = guessNum + 1;		// incrementing guess counter
  }
  return 0;
}
