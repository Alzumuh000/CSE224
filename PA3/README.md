- Muhamad Al-zughir
- CSE224
- 10/29/2021    

This is the directory for the PA3 assignment. Contained in it is the source code for the high/low game and also a makefile.
The rules of the game are that a player gets to think of a number within a chosen range (default is 0-100). In response, the
computer will try to guess the number that was picked by the player using a divide-and-conquer algorithm. Eventually, the
computer will guess the correct number.   
