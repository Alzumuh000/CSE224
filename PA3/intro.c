#include "main.h"
void intro() {
  printf("\nGreetings! Welcome to the High/Low game.\nIn this game, the user has to think of a\nnumber within a range (specified below).\nThe computer will try to guess the correct\nnumber, and the user will enter either 'h'\n(if the guessed number is too high), 'l'\n(if the guessed number is too low), or 'c'\n(if the guessed number is correct).\n\n");
}
