#include "main.h"

int predict(int low, int high) {
  double numGuesses = 1 + (log10(1 + high - low)/log10(2)); 
  return (int)numGuesses;
}
